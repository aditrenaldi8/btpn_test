import { BtpnTestPage } from './app.po';

describe('btpn-test App', () => {
  let page: BtpnTestPage;

  beforeEach(() => {
    page = new BtpnTestPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
