import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Sistem Manajemen Pegawai';

  list:Object[];

  employee:object = {
    "id": 0,
    "name": "",
    "address": "",
    "phone": "",
    "division": "",
    "biography": ""
  };

    constructor(private data: DataService) { }
    
    ngOnInit() {
      this.list = this.data.getEmployeeList();
    }

    addEmployee(obj:object){     
      if(obj['edit'] == 0){
        this.data.addEmployee(obj);
      }else{
        this.data.editEmployee(obj);
      }

      this.employee = {
        "id": 0,
        "name": "",
        "address": "",
        "phone": "",
        "division": "",
        "biography": "",
      };
    }

    remove(id){
      this.data.removeEmployee(id);
    }

    edit(id = 0){
      if(id != 0){
        this.employee = this.data.getEmployee(id);
      }
    }
}
