import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.css']
})
export class FormInputComponent implements OnInit {

  constructor() { }
  
  ngOnInit() {
  
  }


  @Input('parent') list: object[];
  @Input('parent_') employee;

  temporary : Object = this.employee;

  @Output() goToParent: EventEmitter<number> = new EventEmitter<number>();

  onSubmit = function(user){
    console.log(user);
    let id = 0;
    let obj = {};

    if(this.list != ""){
      id = this.list[this.list.length - 1]["id"] + 1;
    }else{
      id = 1 ;
    }
    obj = {
      "id": id,
      "name": user.name,
      "address": user.address,
      "phone": user.phone,
      "division": user.division,
      "biography": user.biography,
      "edit" : user.id,
    };
    if(user.name != "" && user.address != "" && user.phone != "" && user.division != ""){
      this.goToParent.emit(obj);
    }
  }
}
