import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  constructor() { }
  
  ngOnInit() {
   
  }

  @Input('parent') list: object[];
  @Output() goToParent1: EventEmitter<number> = new EventEmitter<number>();
  @Output() goToParent2: EventEmitter<number> = new EventEmitter<number>();

  remove(id){
    this.goToParent1.emit(id);
  }

  edit(id){
    this.goToParent2.emit(id);
  }
}
