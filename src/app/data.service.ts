import { Injectable } from '@angular/core';

@Injectable()
export class DataService {

  //data pegawai json
  private EmployeeList : Object[] = [{
    "id": 1,
    "name":"Aditya Renaldi",
    "address":"Griya Cempaka Arum Cirebon",
    "phone":"081221432980",
    "division":"IT",
    "biography":"Lahir di Cirebon, 3 Desember 1993",
  }];

  constructor() { }

  getEmployeeList():object[]
  {
    return this.EmployeeList;
  }

  getEmployee(id:number):object
  {
    var obj:object;
    for (var i = 0; i < this.EmployeeList.length; i++) {
      if (this.EmployeeList[i]["id"] == id) {
        break;
      } 
    }
    return this.EmployeeList[i];
  }

  addEmployee(obj:object):object[]
  {
    this.EmployeeList.push(obj);
    return this.EmployeeList;
  }

  removeEmployee(id:number):object[]
  {
    for (var i = 0; i < this.EmployeeList.length; i++) {
      if (this.EmployeeList[i]["id"] == id) {
        this.EmployeeList.splice(i, 1);
        break;
      } 
    }
    return this.EmployeeList;
  }

  editEmployee(obj:object):object[]
  {
    for (var i = 0; i < this.EmployeeList.length; i++) {
      if (this.EmployeeList[i]["id"] == obj["id"]) {
        this.EmployeeList[i]["name"] = obj["name"];
        this.EmployeeList[i]["phone"] = obj["phone"];
        this.EmployeeList[i]["address"] = obj["address"];
        this.EmployeeList[i]["division"] = obj["division"];
        this.EmployeeList[i]["biography"] = obj["biography"];
        break;
      } 
    }
    return this.EmployeeList;
  }
}
